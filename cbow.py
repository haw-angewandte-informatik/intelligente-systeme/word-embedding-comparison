import torch
from word_embedder import WordEmbedder
from utils import Vector, Context

# Import one-hot-encoding
import json
with open('oneHotEncoding.json', 'r', encoding='utf-8') as f:
    one_hot_encoding = json.load(f)
input_vector_size = len(one_hot_encoding)

def word2one_hot(word: str) -> torch.Tensor:
    vector = torch.zeros(input_vector_size)
    vector[one_hot_encoding[word]] = 1
    return vector

# Construct one-hot-decoding
one_hot_decoding = {}
for key in one_hot_encoding:
    one_hot_decoding[one_hot_encoding[key]] = key
output_vector_size = input_vector_size

def softmax2word(vector: torch.Tensor) -> str:
    return one_hot_decoding[vector.argmax(dim=0).item()]

import os
import numpy as np
import utils
# Word embedding
map: dict[str, list[Vector]] = {}
for word in one_hot_encoding:
    map[one_hot_encoding[word]] = []


embedder = WordEmbedder(4, one_hot_encoding).to(utils.DEVICE)

all_files = os.listdir('inputs')
print(utils.timestamp()+"Found " + str(len(all_files)) + " files.")

BATCH_SIZE = 64
input_batch: list[list[str]] = []
target_batch: list[str] = []
for file in all_files:
    file = os.path.join('inputs', file)
    content = utils.file2str(file)
    tokens: list[str] = utils.tokenize(content)
    print(utils.timestamp()+"Running file " + file + " with "+str(len(tokens))+" tokens...")
    # First two tokens manually to make missing contex words zero
    target = tokens[0]
    context = ["✌", "✌", tokens[1], tokens[2]]
    input_batch.append(context)
    target_batch.append(target)
    if len(input_batch) == BATCH_SIZE - 1:
        embedder.train_batch(input_batch, target_batch, True)
        input_batch = []
        target_batch = []
    target = tokens[1]
    context = ["✌", tokens[0], tokens[2], tokens[3]]
    input_batch.append(context)
    target_batch.append(target)
    if len(input_batch) == BATCH_SIZE - 1:
        embedder.train_batch(input_batch, target_batch, True)
        input_batch = []
        target_batch = []
    for i in range(2, len(tokens) - 2):
        target = tokens[i]
        context = [tokens[i-2], tokens[i-1], tokens[i+1], tokens[i+2]]
        input_batch.append(context)
        target_batch.append(target)
        if len(input_batch) == BATCH_SIZE - 1:
            embedder.train_batch(input_batch, target_batch, True)
            input_batch = []
            target_batch = []
    # Last two manually to set missing context words zero
    target = tokens[-2]
    context = [tokens[-4], tokens[-3], tokens[-1], "✌"]
    if len(input_batch) == BATCH_SIZE - 1:
        embedder.train_batch(input_batch, target_batch, True)
        input_batch = []
        target_batch = []
    target = tokens[-1]
    context = [tokens[-3], tokens[-2], "✌", "✌"]
    if len(input_batch) == BATCH_SIZE - 1:
        embedder.train_batch(input_batch, target_batch, True)
        input_batch = []
        target_batch = []
if len(input_batch) > 0:
    embedder.train_batch(input_batch, target_batch, True)
    input_batch = []
    target_batch = []

# Same but gather train outputs in a list in the map for each word
print(utils.timestamp()+"First pass done, starting second pass...")
input_batch: list[list[str]] = []
target_batch: list[str] = []
for file in all_files:
    file = os.path.join('inputs', file)
    content = utils.file2str(file)
    tokens = utils.tokenize(content)
    timestamp = str(np.datetime64('now'))
    print(utils.timestamp()+"Running file " + file + " with "+str(len(tokens))+" tokens...")
    # First two tokens manually to make missing contex words zero
    target = tokens[0]
    context = ["✌", "✌", tokens[1], tokens[2]]
    if len(input_batch) == BATCH_SIZE - 1:
        hidden_vectors = embedder.train_batch(input_batch, target_batch, True)
        for i in range(len(hidden_vectors)):
            map[target_batch[i]].append(hidden_vectors[i])
        input_batch = []
        target_batch = []
    target = tokens[1]
    context = ["✌", tokens[0], tokens[2], tokens[3]]
    if len(input_batch) == BATCH_SIZE - 1:
        hidden_vectors = embedder.train_batch(input_batch, target_batch, True)
        for i in range(len(hidden_vectors)):
            map[target_batch[i]].append(hidden_vectors[i])
        input_batch = []
        target_batch = []
    for i in range(2, len(tokens) - 2):
        target = tokens[i]
        context = [tokens[i-2], tokens[i-1], tokens[i+1], tokens[i+2]]
        if len(input_batch) == BATCH_SIZE - 1:
            hidden_vectors = embedder.train_batch(input_batch, target_batch, True)
            for i in range(len(hidden_vectors)):
                map[target_batch[i]].append(hidden_vectors[i])
            input_batch = []
            target_batch = []
    # Last two manually to set missing context words zero
    target = tokens[-2]
    context = [tokens[-4], tokens[-3], tokens[-1], "✌"]
    if len(input_batch) == BATCH_SIZE - 1:
        hidden_vectors = embedder.train_batch(input_batch, target_batch, True)
        for i in range(len(hidden_vectors)):
            map[target_batch[i]].append(hidden_vectors[i])
        input_batch = []
        target_batch = []
    target = tokens[-1]
    context = [tokens[-3], tokens[-2], "✌", "✌"]
    if len(input_batch) == BATCH_SIZE - 1:
        hidden_vectors = embedder.train_batch(input_batch, target_batch, True)
        for i in range(len(hidden_vectors)):
            map[target_batch[i]].append(hidden_vectors[i])
        input_batch = []
        target_batch = []
if len(input_batch) > 0:
    hidden_vectors = embedder.train_batch(input_batch, target_batch, True)
    for i in range(len(hidden_vectors)):
        map[target_batch[i]].append(hidden_vectors[i])
    input_batch = []
    target_batch = []

# Calculate average for each word
embedding = {word: np.mean(map[word], axis=0) for word in map}
# write embedding to cbowEmbeddings.json
with open('cbowEmbeddings.json', 'w', encoding='utf-8') as f:
    json.dump(embedding, f, ensure_ascii=False, indent=4)