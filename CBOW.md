# Continuous Bag Of Words (CBOW)

## Ablauf

### One-hot encoding einlesen

Eine Datei mit dem folgenden Format wird als Mapping eingelesen:

```
[
    {"Hey": 0},
    {"Some": 1},
    {"Wort": 2},
    {"Next": 3},
    [...]
    {"$": 50000}
]
```
Der Wert der Map stellt den Index der 1 im One-hot-Vektor dar.

### Word embedding trainieren

Mehrere Texte (z.B. Bücher oder Code-Dateien) werden eingelesen und nacheinander in den CBOW-Algorithmus gegeben.  

Es werden jeweils das Wort vor dem aktuellen Ziel-Wort und das Wort danach als Input verwendet. Sie werden mit dem One-hot Encoding gemappt und in ein flaches neuronales Netz (nur ein hidden-layer) gegeben.

```
Text: Hey, this is a great party and after this we will go home.

var ohEncodings = getOneHotEncodings();
var context = ["$👌Textanfang🎉$", ","]
var vec = hidden.forward(context.map(word => ohEncodings[word])); // Size 100
map["Hey"].add(vec);
// map = {"Hey": [vec1]}
var prediction = output.forward(vec); // Size 50000
var target = ohEncodings["Hey"];
var loss = lossFunction(prediction, target);
var vecTarget = output.backward(loss);
hidden.backward(vecTarget);

context = ["Hey", "this"]
vec = hidden.forward(context.map(word => ohEncodings[word])); // Size 100
map[","].add(vec);
// map = {"Hey": [vec1], ",": [vec2]}
prediction = output.forward(vec); // Size 50000
target = ohEncodings[","];
loss = lossFunction(prediction, target);
vecTarget = output.backward(loss);
hidden.backward(vecTarget);

context = [",", "is"]
vec = hidden.forward(context.map(word => ohEncodings[word])); // Size 100
map["this"].add(vec);
// map = {"Hey": [vec1], ",": [vec2], "this": [vec3]}
prediction = output.forward(vec); // Size 50000
target = ohEncodings["this"];
loss = lossFunction(prediction, target);
vecTarget = output.backward(loss);
hidden.backward(vecTarget);

...

context = ["after", "we"];
vec = hidden.forward(context.map(word => ohEncodings[word])); // Size 100
map["this"].add(vec);
// map = {
    "Hey": [vec1], 
    ",": [vec2], 
    "this": [vec3, vec4]
    }
prediction = output.forward(vec); // Size 50000
target = ohEncodings["this"];
loss = lossFunction(prediction, target);
vecTarget = output.backward(loss);
hidden.backward(vecTarget);

...

var embeddings = map.map((word, vecs) => {
    var sum = vecs.reduce((acc, vec) => acc.add(vec), new Vector(100));
    return sum.divide(vecs.length);
});

```