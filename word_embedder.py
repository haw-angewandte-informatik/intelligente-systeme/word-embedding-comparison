import torch
import torch.nn as nn
import torch.nn.functional as F

from utils import Vector, DEVICE

class WordEmbedder(nn.Module):
    def __init__(self, context_size: int, ohEncodings: dict[str, int]):
        super(WordEmbedder, self).__init__()
        self.context_size = context_size
        self.ohEncodings = ohEncodings
        self.input_vector_size = len(ohEncodings)
        # 1 Hidden-Layer mit 384 Neuronen, 
        self.hidden_layer = nn.Linear(self.input_vector_size, 384).to(device=DEVICE)
        # default rand
        self.hidden_layer.weight.data = torch.randn(self.hidden_layer.weight.data.shape).to(device=DEVICE)
        self.output_layer = nn.Linear(384, self.input_vector_size).to(device=DEVICE)
        self.output_layer.weight.data = torch.randn(self.output_layer.weight.data.shape).to(device=DEVICE)
        # Optimizer
        self.optimizer = torch.optim.SGD(self.parameters(), lr=0.001)

    def word2one_hot(self, word: str) -> torch.Tensor:
        vector = torch.zeros(self.input_vector_size)
        vector[self.ohEncodings[word]] = 1
        return vector.to(device=DEVICE)

    def forward(self, inputs: list[str]) -> Vector:
        # Make sure inputs has the same size as input layers
        assert len(inputs) == self.context_size
        # For each index in indices, build the corresponding one-hot-encoded vector, run it through the hidden layer and average the results
        hidden_results = [self.hidden_layer(self.word2one_hot(word) if word in self.ohEncodings else torch.zeros(self.input_vector_size).to(device=DEVICE)) for word in inputs]
        # Average the results
        hidden_vector = F.relu(torch.mean(torch.stack(hidden_results), axis=0))
        # Run the hidden vector through the output layer
        return F.softmax(self.output_layer(hidden_vector), dim=0).toList()
    
    # Return hidden_vector as vector (list of floats)
    def train(self, inputs: list[str], target: str, doBackprop: bool) -> Vector:
        # Make sure inputs has the same size as input layers
        assert len(inputs) == self.context_size, f"len(inputs)={len(inputs)} != {self.context_size}"
        # Make sure target is in one-hot-encoding
        assert target in self.ohEncodings, f"target={target} not in one-hot-encoding"
        # Calculate loss using neg loss
        loss = nn.NLLLoss()
        # For each index in indices, build the corresponding one-hot-encoded vector, run it through the hidden layer and average the results
        hidden_results = [self.hidden_layer(self.word2one_hot(word) if word in self.ohEncodings else torch.zeros(self.input_vector_size).to(device=DEVICE)) for word in inputs]
        # Average the results
        hidden_vector = F.relu(torch.mean(torch.stack(hidden_results), axis=0))
        # Run the hidden vector through the output layer
        output_vector = self.output_layer(hidden_vector)
        # Calculate loss
        loss = loss(output_vector, self.word2one_hot(target))
        if doBackprop:
            # Backpropagate
            loss.backward()
            # Update weights
            self.optimizer.step()
            # Reset gradients
            self.optimizer.zero_grad()
            
        # Return hidden_vector as vector (list of floats)
        return hidden_vector.tolist()
    
    def train_batch(self, inputs: list[list[str]], targets: list[str], doBackprop: bool) -> list[Vector]:
        assert len(inputs) == len(targets), "Number of inputs and targets should be the same."

        batch_size = len(inputs)
        input_vectors: list[torch.Tensor] = []
        target_vectors: list[torch.Tensor] = []

        # Convert inputs and targets to vectors
        for i in range(batch_size):
            input_vector = [self.word2one_hot(word) if word in self.ohEncodings else torch.zeros(self.input_vector_size).to(device=DEVICE) for word in inputs[i]]
            target_vector = self.word2one_hot(targets[i])

            input_vectors.append(input_vector)
            target_vectors.append(target_vector)

        # Convert vectors to tensors
        input_tensors = [torch.stack(input_vector) for input_vector in input_vectors]
        target_tensors = torch.stack(target_vectors)

        # Calculate loss using negative log-likelihood loss
        loss_fn = nn.CrossEntropyLoss()

        # Forward pass
        hidden_vectors = [F.relu(torch.mean(self.hidden_layer(input_tensor), dim=0)) for input_tensor in input_tensors]
        output_tensors = torch.stack([self.output_layer(hidden_vector) for hidden_vector in hidden_vectors])

        # Calculate loss
        loss = loss_fn(output_tensors, target_tensors)

        if doBackprop:
            # Backpropagation
            loss.backward()
            # Update weights
            self.optimizer.step()
            # Reset gradients
            self.optimizer.zero_grad()

        # Return hidden vectors as list of lists
        return [hidden_vector.tolist() for hidden_vector in hidden_vectors]
