import os
import sys
import re
import torch

regex = re.compile('[0-9]+[,.][0-9]+[!\?,\.\(\)\n\t\'\_\$\%\&]*|[a-zA-Z0-9äöüÄÖÜßáàóòúùéèíì\-\'\_\$\%\&]+|[\"!\?,\.\(\)\n\t\/]')
DEVICE = (
    "cuda"
    if torch.cuda.is_available()
    else "mps"
    if torch.backends.mps.is_available()
    else "cpu"
)
print("Using device: " + DEVICE)

# tokenize a text coming in as a single string
def tokenize(text: str) -> list:
    return regex.findall(text)

# read file, tokenize it, put everything in a set to remove duplicates
def file2vocab(filename: str) -> set:
    file_data = open(filename, "r", encoding="utf-8").read()
    return set(tokenize(file_data))

# function to convert epub file to plain text as str
import epub_conversion
def epub2vocab(filename: str) -> set:
    from epub_conversion.utils import open_book, convert_epub_to_lines
    book = open_book(filename)
    lines = convert_epub_to_lines(book)
    return set(tokenize("\n".join(lines)))

# read all files and put everything in a single set
def files2vocab(files: list[str]) -> set:
    all_words = set()
    for file in files:
        all_words = all_words.union(file2vocab(file) if file.endswith('.txt') else (epub2vocab(file) if file.endswith('.epub') else set()))
    return all_words

# read file, tokenize it
def txt2str(filename: str) -> str:
    file_data = open(filename, "r", encoding="utf-8").read()
    return file_data

# function to convert epub file to plain text as str
from epub_conversion.utils import open_book, convert_epub_to_lines
def epub2str(filename: str) -> str:
    book = open_book(filename)
    lines = convert_epub_to_lines(book)
    return "\n".join(lines)

def file2str(filename: str) -> str:
    if filename.endswith(".epub"):
        return epub2str(filename)
    else:
        return txt2str(filename)
    
import datetime
def timestamp():
    return datetime.datetime.now().strftime("[%H:%M:%S] ")

def find_all_files():
    return ["GeschichteDerPhilosophieImIslam.txt"] #os.listdir('inputs')

# type aliases
Vector = list[float]
Context = list[str]