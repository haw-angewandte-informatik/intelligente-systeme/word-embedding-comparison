# Word embedding comparison

## TODO
- Vorhandene Transformer Implementierung finden und einbinden -> **huggingface/transformers** liefert fertige Tranformer-Modelle, denen wir unsere eigenen Embeddings geben können.
- Liste von Word Embeddings aufstellen
- Drei Word Embeddings auswählen und vergleichen -> [CBOW, Skip Gram, FastText]
- Trainings-Texte auswählen -> Bücher, Repositories (explizit Dateiendungen auswählen)
- Testbench aufbauen


## Ablauf

### One-hot encoding

- Über den gesamten Input iterieren
- Jedes Token (Wort, Satzzeichen) bekommt einen Index im One-hot-Vektor zugeordnet (Textanfang = 0, Textende = 0)

Ergebnis:
[
    {"Wort": 3},
    {"Next": 4},
    [...]
]

### Word embedding trainieren

siehe [CBOW](./CBOW.md)

Ergebnis ist wieder ein Mapping vom Wort zum zugehörigen (niedrigdimensionalen, z.B. 100) Embedding-Vektor:
[
    {"Wort": Vector},
    {"Next": Vector},
    [...]
]

### Embeddings testen

Jedes Embedding wird nun mit denselben Prompts getestet, indem es in dieselbe Tranformer-Architektur gepackt und dann als Input ein Vergleichstext benutzt wird.  
Die Ergebnisse werden verglichen.